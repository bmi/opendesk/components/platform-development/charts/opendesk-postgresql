## [2.1.2](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-postgresql/compare/v2.1.1...v2.1.2) (2025-01-10)


### Bug Fixes

* Set `max_connections` based on the sum of the user connection limits ([198983e](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-postgresql/commit/198983e52e1efd93bba5696a7f564899731b884b))

## [2.1.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-postgresql/compare/v2.1.0...v2.1.1) (2024-08-19)


### Bug Fixes

* User specific connection support. ([b999e89](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-postgresql/commit/b999e89b08828bfead3143606eca0db65bdbd691))

# [2.1.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-postgresql/compare/v2.0.6...v2.1.0) (2024-08-15)


### Features

* **init:** Support for user based connection limits and password update. ([b9be1a8](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-postgresql/commit/b9be1a83721e0b2f16f4eea335dd22eee9451a74))

## [2.0.6](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-postgresql/compare/v2.0.5...v2.0.6) (2023-12-21)


### Bug Fixes

* Add GPG key, update README.md ([bd641cc](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-postgresql/commit/bd641cc31a7ee78db3346acbb84c471f8ccdf227))

## [2.0.5](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-postgresql/compare/v2.0.4...v2.0.5) (2023-12-20)


### Bug Fixes

* **ci:** Add .common:tags: [] ([5f5d204](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-postgresql/commit/5f5d2044836df07964b2d77ac8713aaf04083c24))

## [2.0.4](https://gitlab.opencode.de/bmi/opendesk/components/charts/opendesk-postgresql/compare/v2.0.3...v2.0.4) (2023-12-20)


### Bug Fixes

* **ci:** Move repo to Open CoDE ([ea6f4a2](https://gitlab.opencode.de/bmi/opendesk/components/charts/opendesk-postgresql/commit/ea6f4a22c0a6e1d442d1d0da0bcf733a894f0c4d))

## [2.0.3](https://gitlab.souvap-univention.de/souvap/tooling/charts/postgresql/compare/v2.0.2...v2.0.3) (2023-10-25)


### Bug Fixes

* **postgresql:** Use "| quote" wherever possible ([ec66289](https://gitlab.souvap-univention.de/souvap/tooling/charts/postgresql/commit/ec662890d0bdf825efca8380f28ea82e0670fcc5))

## [2.0.2](https://gitlab.souvap-univention.de/souvap/tooling/charts/postgresql/compare/v2.0.1...v2.0.2) (2023-08-27)


### Bug Fixes

* **postgresql:** Enhance the default security settings ([d5ac3ea](https://gitlab.souvap-univention.de/souvap/tooling/charts/postgresql/commit/d5ac3ea62ea58c80d85fa2527d47cc4788e0c1cf))

## [2.0.1](https://gitlab.souvap-univention.de/souvap/tooling/charts/postgresql/compare/v2.0.0...v2.0.1) (2023-08-03)


### Bug Fixes

* **postgresql:** Add labels to job ([66c4049](https://gitlab.souvap-univention.de/souvap/tooling/charts/postgresql/commit/66c4049317f4987b07dafe6bb82795bff9a9834f))

# [2.0.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/postgresql/compare/v1.1.1...v2.0.0) (2023-07-30)


### Features

* **postgresql:** Enhance templateable options. ([f53f0c1](https://gitlab.souvap-univention.de/souvap/tooling/charts/postgresql/commit/f53f0c1b934ff55a6d4c83593d536771e3d0f442))


### BREAKING CHANGES

* **postgresql:** Move .Values.configuration to .Values.postgres.configuration
